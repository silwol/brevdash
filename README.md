# Generate static website reports for characteristics over time

The `brevdash` tool can manage time series of numeric or boolean
characteristics and generate static website reports with plots showing how
they evolve over time.

## Example usage

Create the repository:
```
~$ mkdir mydata
~$ cd mydata
~/mydata$ brevdash init --name "My Data"
```

Add the core data:
```
~/mydata$ brevdash add characteristic number_of_warnings --name "Number of warnings" --datatype integer
~/mydata$ brevdash add project my_project --name "My project"
```

Add some datapoints:
```
~/mydata$ brevdash add datavalue --characteristic number_of_warnings --project my_project --value 15 --date 2021-03-01
~/mydata$ brevdash add datavalue --characteristic number_of_warnings --project my_project --value 17 --date 2021-03-08
~/mydata$ brevdash add datavalue --characteristic number_of_warnings --project my_project --value 9 --date 2021-03-15
```

Generate the html report:
```
~/mydata$ brevdash report --output-path ../report-html
```

## Repository format

The format of the repository is a directory tree containing `toml` files
with the data. There is no detailed description of the format yet, but it
is rather easy to find out by either reading the brevdash-data source code,
or using the `brevdash` command-line tool for manipulating and looking at
the generated files.

## Installation

Installation works as with any typical Rust project:

```
cargo install brevdash
```

Tested with Debian GNU/Linux 11, but should work with most other Linux
distributions just as easy. Maybe you'll have to install some system
libraries first. Please let me know when you stumble over problems you
can't solve.
