use crate::data;
use anyhow::{bail, Result};
use std::path::Path;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct Args {
    /// The id of the characteristic which gets added (e.g. "my_characteristic")
    characteristic_id: String,

    /// The human-readable name of the characteristic (e.g. "My Characteristic")
    #[structopt(long)]
    name: String,

    /// The datatype of the characteristic (boolean or integer)
    #[structopt(long)]
    datatype: data::DataType,
}

impl Args {
    pub fn process(&self, working_directory: &Path) -> Result<()> {
        let mut repo = data::Repository::open(working_directory)?;

        if repo
            .description
            .characteristics
            .contains_key(&self.characteristic_id)
        {
            bail!(
                "A project with the id {:?} already exists",
                self.characteristic_id
            );
        }

        let characteristic = data::CharacteristicDescription {
            name: self.name.to_string(),
            datatype: self.datatype,
        };
        repo.description
            .characteristics
            .insert(self.characteristic_id.clone(), characteristic);

        repo.store_description()?;

        Ok(())
    }
}
