use crate::Result;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct Args {
    outdir: PathBuf,
    shell: clap::Shell,
}

impl Args {
    pub fn process(&self) -> Result<()> {
        println!("Writing shell completion files to {:?}", self.outdir);
        std::fs::create_dir_all(&self.outdir)?;
        let mut app = crate::Command::clap();
        app.gen_completions(crate::PROGRAM_NAME, self.shell, &self.outdir);
        Ok(())
    }
}
