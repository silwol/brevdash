use anyhow::{Context, Result};
use askama::Template;
use brevdash_data as data;
use chrono::{naive::NaiveDate, Utc};
use log::debug;
use plotters::prelude::{
    BitMapBackend, ChartBuilder, Color, IntoDrawingArea, LineSeries,
    ShapeStyle, BLACK, BLUE, WHITE,
};
use std::collections::BTreeMap;
use std::fs::File;
use std::io::Write;
use std::path::{Path, PathBuf};
use structopt::StructOpt;

const CHART_WIDTH: u32 = 800;
const CHART_HEIGHT_INTEGER: u32 = 400;
const CHART_HEIGHT_BOOLEAN: u32 = 200;
const CHART_X_LABEL_AREA_SIZE: u32 = 30;
const CHART_Y_LABEL_AREA_SIZE: u32 = 60;
const CHART_LABEL_FONT_FACE: &str = "Noto Mono";
const CHART_LABEL_FONT_SIZE: u32 = 12;

#[derive(Debug, StructOpt)]
pub struct Args {
    /// Output directory for the report
    #[structopt(
        parse(from_os_str),
        long,
        default_value = "brevdash-report"
    )]
    output_path: PathBuf,
}

impl Args {
    pub fn process(&self, working_directory: &Path) -> Result<()> {
        let repo = data::Repository::open(working_directory)?;

        debug!("Brevdash repository loaded from {:?}", &working_directory);

        let project_names = repo
            .load_project_descriptions()?
            .into_iter()
            .map(|(k, v)| (k, v.name))
            .collect();

        std::fs::create_dir_all(&self.output_path)?;

        {
            let template = IndexTemplate {
                programinfo: ProgramInfo::default(),
                root: &repo.description,
                projects: &project_names,
            };
            let mut file =
                File::create(self.output_path.join("index.html"))?;
            file.write_all(template.render()?.as_bytes())?;
        }

        std::fs::create_dir_all(self.output_path.join("project"))?;
        std::fs::create_dir_all(self.output_path.join("characteristic"))?;
        std::fs::create_dir_all(self.output_path.join("charts"))?;
        std::fs::create_dir_all(self.output_path.join("artifacts"))?;

        for project_id in project_names.keys() {
            let project_description =
                repo.load_project_description(project_id)?;
            let characteristics = repo
                .load_project_characteristics_by_project_id(project_id)?;

            for characteristic in &characteristics {
                let path = self.output_path.join("charts").join(format!(
                    "{}_{}.png",
                    project_id, characteristic.characteristic_id
                ));
                characteristic.plot_to_file(&path)?;

                if let Some(date) = characteristic.latest_date() {
                    let artifacts =
                        characteristic.latest_entry_artifacts();
                    if !artifacts.is_empty() {
                        std::fs::create_dir_all(
                            self.output_path.join("artifacts"),
                        )?;
                    }
                    for artifact in artifacts {
                        let source_path = repo
                        .project_datapoint_characteristic_artifact_path(
                            project_id,
                            date,
                            &characteristic.characteristic_id,
                            &artifact,
                        );
                        let target_directory = self
                            .output_path
                            .join("artifacts")
                            .join(format!(
                                "{}_{}",
                                characteristic.project_id,
                                characteristic.characteristic_id
                            ));
                        std::fs::create_dir_all(&target_directory)?;
                        let target_path = target_directory.join(artifact);
                        std::fs::copy(source_path, target_path)?;
                    }
                }
            }

            let template = ProjectTemplate {
                programinfo: ProgramInfo::default(),
                root: repo.description.clone(),
                project_id: project_id.to_string(),
                description: project_description.clone(),
                characteristics,
            };
            let mut file = File::create(
                self.output_path
                    .join("project")
                    .join(format!("{}.html", project_id)),
            )?;
            file.write_all(template.render()?.as_bytes())?;
        }

        for (characteristic_id, characteristic) in
            &repo.description.characteristics
        {
            let projects = repo
                .load_project_characteristics_by_characteristic_id(
                    characteristic_id,
                )?;

            let template = CharacteristicTemplate {
                programinfo: ProgramInfo::default(),
                root: &repo.description,
                characteristic_id,
                description: &characteristic,
                projects,
            };

            let mut file = File::create(
                self.output_path
                    .join("characteristic")
                    .join(format!("{}.html", characteristic_id)),
            )?;
            file.write_all(template.render()?.as_bytes())?;
        }
        Ok(())
    }
}

#[derive(Debug, Template)]
#[template(path = "index.html")]
struct IndexTemplate<'a> {
    programinfo: ProgramInfo,
    root: &'a data::RootDescription,
    projects: &'a BTreeMap<String, String>,
}

#[derive(Debug, Template)]
#[template(path = "project.html")]
struct ProjectTemplate {
    programinfo: ProgramInfo,
    root: data::RootDescription,
    project_id: String,
    description: data::ProjectDescription,
    characteristics: Vec<ProjectCharacteristic>,
}

#[derive(Debug, Template)]
#[template(path = "characteristic.html")]
struct CharacteristicTemplate<'a> {
    programinfo: ProgramInfo,
    root: &'a data::RootDescription,
    characteristic_id: &'a str,
    description: &'a data::CharacteristicDescription,
    projects: Vec<ProjectCharacteristic>,
}

#[derive(Debug)]
struct ProgramInfo {
    name: &'static str,
    version: &'static str,
    homepage: &'static str,
}

impl Default for ProgramInfo {
    fn default() -> Self {
        Self {
            name: crate::PROGRAM_NAME,
            version: crate::PROGRAM_VERSION,
            homepage: crate::PROGRAM_HOMEPAGE,
        }
    }
}

trait RepositoryExt {
    fn load_project_characteristic(
        &self,
        project_id: &str,
        characteristic_id: &str,
    ) -> Result<ProjectCharacteristic>;

    fn load_project_characteristics_by_project_id(
        &self,
        project_id: &str,
    ) -> Result<Vec<ProjectCharacteristic>>;

    fn load_project_characteristics_by_characteristic_id(
        &self,
        characteristic_id: &str,
    ) -> Result<Vec<ProjectCharacteristic>>;
}

impl RepositoryExt for data::Repository {
    fn load_project_characteristic(
        &self,
        project_id: &str,
        characteristic_id: &str,
    ) -> Result<ProjectCharacteristic> {
        let project_description =
            self.load_project_description(project_id)?;
        let characteristic_description = self
            .description
            .characteristics
            .get(&characteristic_id.to_string())
            .with_context(|| {
                format!(
                    "Characteristic description with id {:?} not found",
                    characteristic_id
                )
            })?
            .clone();
        let raw_series = self.load_project_datapoints(project_id)?;
        let series = raw_series
            .into_iter()
            .map(|(id, datapoint)| {
                (
                    id,
                    datapoint.get(&characteristic_id.to_string()).cloned(),
                )
            })
            .collect();
        Ok(ProjectCharacteristic {
            project_id: project_id.to_string(),
            project_description,
            characteristic_id: characteristic_id.to_string(),
            characteristic_description,
            series,
        })
    }

    fn load_project_characteristics_by_project_id(
        &self,
        project_id: &str,
    ) -> Result<Vec<ProjectCharacteristic>> {
        let mut items = Vec::new();
        for characteristic_id in self.description.characteristics.keys() {
            items.push(self.load_project_characteristic(
                project_id,
                characteristic_id,
            )?);
        }
        Ok(items)
    }

    fn load_project_characteristics_by_characteristic_id(
        &self,
        characteristic_id: &str,
    ) -> Result<Vec<ProjectCharacteristic>> {
        let mut items = Vec::new();
        for project_id in &self.load_project_ids()? {
            items.push(self.load_project_characteristic(
                project_id,
                characteristic_id,
            )?);
        }
        Ok(items)
    }
}

#[derive(Debug)]
struct ProjectCharacteristic {
    project_id: String,
    project_description: data::ProjectDescription,
    characteristic_id: String,
    characteristic_description: data::CharacteristicDescription,
    series: BTreeMap<NaiveDate, Option<data::DataEntry>>,
}

impl ProjectCharacteristic {
    fn latest_date(&self) -> Option<NaiveDate> {
        self.series.keys().rev().next().cloned()
    }

    fn latest_entry(&self) -> Option<data::DataEntry> {
        match self.series.iter().rev().next() {
            Some((_k, v)) => v.clone(),
            None => None,
        }
    }

    fn latest_value(&self) -> Option<data::DataValue> {
        self.latest_entry().map(|e| e.value)
    }

    fn latest_value_boolean(&self) -> Option<bool> {
        self.latest_value().map(|v| v.boolean()).flatten()
    }

    fn latest_value_integer(&self) -> Option<i64> {
        self.latest_value().map(|v| v.integer()).flatten()
    }

    fn latest_entry_artifacts(&self) -> Vec<PathBuf> {
        match self.latest_entry() {
            Some(e) => e.artifacts,
            None => Vec::new(),
        }
    }

    fn plot_to_file(&self, path: &Path) -> Result<()> {
        match self.characteristic_description.datatype {
            data::DataType::Boolean => {
                self.plot_boolean_characteristic_to_file(path)
            }
            data::DataType::Integer => {
                self.plot_integer_characteristic_to_file(path)
            }
        }
    }

    fn get_entry(&self, date: NaiveDate) -> Option<data::DataEntry> {
        self.series.get(&date).cloned().flatten()
    }

    fn get_value(&self, date: NaiveDate) -> Option<data::DataValue> {
        self.get_entry(date).map(|e| e.value)
    }

    fn get_value_boolean(&self, date: NaiveDate) -> Option<bool> {
        self.get_value(date).map(|v| v.boolean()).flatten()
    }

    fn get_value_integer(&self, date: NaiveDate) -> Option<i64> {
        self.get_value(date).map(|v| v.integer()).flatten()
    }

    fn extract_series_boolean(&self) -> BTreeMap<NaiveDate, Option<bool>> {
        self.series
            .keys()
            .map(|k| (*k, self.get_value_boolean(*k)))
            .collect()
    }

    fn extract_series_integer(&self) -> BTreeMap<NaiveDate, Option<i64>> {
        self.series
            .keys()
            .map(|k| (*k, self.get_value_integer(*k)))
            .collect()
    }

    fn plot_integer_characteristic_to_file(
        &self,
        path: &Path,
    ) -> Result<()> {
        let root =
            BitMapBackend::new(path, (CHART_WIDTH, CHART_HEIGHT_INTEGER))
                .into_drawing_area();
        root.fill(&WHITE)?;
        let now = Utc::today().naive_local();
        let series = self.extract_series_integer();
        let start = series.keys().next().unwrap_or(&now).pred();
        let end = series.keys().rev().next().unwrap_or(&now).succ();
        let min =
            series.values().filter_map(|e| *e).min().unwrap_or(-1i64);
        let max = series.values().filter_map(|v| *v).max().unwrap_or(1i64);
        let mut chart = ChartBuilder::on(&root)
            .margin(5)
            .x_label_area_size(CHART_X_LABEL_AREA_SIZE)
            .top_x_label_area_size(CHART_X_LABEL_AREA_SIZE)
            .y_label_area_size(CHART_Y_LABEL_AREA_SIZE)
            .right_y_label_area_size(CHART_Y_LABEL_AREA_SIZE)
            .build_cartesian_2d(
                start..end,
                std::cmp::min(0, min) - 1..(std::cmp::max(0, max) + 1),
            )?;

        chart
            .configure_mesh()
            .label_style((CHART_LABEL_FONT_FACE, CHART_LABEL_FONT_SIZE))
            .draw()?;

        chart.draw_series(LineSeries::new(
            vec![(start, 0), (end, 0)],
            ShapeStyle::from(&BLACK.mix(0.3)).stroke_width(2),
        ))?;

        chart.draw_series(LineSeries::new(
            series.iter().filter_map(|(k, v)| match v {
                Some(v) => Some((*k, *v)),
                None => None,
            }),
            ShapeStyle::from(&BLUE).stroke_width(2),
        ))?;

        Ok(())
    }

    fn plot_boolean_characteristic_to_file(
        &self,
        path: &Path,
    ) -> Result<()> {
        let root =
            BitMapBackend::new(path, (CHART_WIDTH, CHART_HEIGHT_BOOLEAN))
                .into_drawing_area();
        root.fill(&WHITE)?;
        let now = Utc::today().naive_local();
        let series = self.extract_series_boolean();
        let start = series.keys().next().unwrap_or(&now).pred();
        let end = series.keys().rev().next().unwrap_or(&now).succ();
        let mut chart = ChartBuilder::on(&root)
            .margin(5)
            .x_label_area_size(CHART_X_LABEL_AREA_SIZE)
            .top_x_label_area_size(CHART_X_LABEL_AREA_SIZE)
            .y_label_area_size(CHART_Y_LABEL_AREA_SIZE)
            .right_y_label_area_size(CHART_Y_LABEL_AREA_SIZE)
            .build_cartesian_2d(start..end, -2..2)?;

        chart
            .configure_mesh()
            .label_style((CHART_LABEL_FONT_FACE, CHART_LABEL_FONT_SIZE))
            .light_line_style(&WHITE)
            .y_label_formatter(&|v| {
                match *v {
                    1 => "Yes",
                    0 => "Unknown",
                    -1 => "No",
                    _ => "",
                }
                .to_string()
            })
            .draw()?;

        chart.draw_series(LineSeries::new(
            series.iter().map(|(k, v)| {
                let v = match v {
                    Some(true) => 1,
                    Some(false) => -1,
                    None => 0,
                };
                (*k, v)
            }),
            ShapeStyle::from(&BLUE).stroke_width(2),
        ))?;

        Ok(())
    }
}
