use anyhow::{bail, Result};
use brevdash_data as data;
use log::debug;
use std::path::Path;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct Args {
    /// The name of the brevdash dashboard, will be used as the title
    #[structopt(long)]
    name: String,
}

impl Args {
    pub fn process(&self, working_directory: &Path) -> Result<()> {
        if working_directory.join("brevdash.toml").exists() {
            bail!(
                "There is already a `brevdash.toml` file present in {:?}",
                working_directory.canonicalize().unwrap()
            );
        }

        let description = data::RootDescription {
            name: self.name.to_string(),
            characteristics: Default::default(),
        };

        debug!("Creating brevdash repository in {:?}", &working_directory);
        data::Repository::create(working_directory, description)?;
        debug!("Brevdash repository created");

        Ok(())
    }
}
