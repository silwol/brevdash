use crate::Result;
use brevdash_data as data;
use chrono::naive::NaiveDate;
use std::path::Path;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct Args {
    /// Remove artifacts since this date included
    #[structopt(long)]
    since: Option<NaiveDate>,

    /// Remove artifacts until this date included
    #[structopt(long)]
    until: Option<NaiveDate>,
}

impl Args {
    pub fn process(&self, working_directory: &Path) -> Result<()> {
        let repo = data::Repository::open(working_directory)?;

        for project_id in repo.load_project_ids()? {
            for (date, datapoint) in
                repo.load_project_datapoints(&project_id)?.into_iter()
            {
                if self.should_remove_artifacts_for_date(date) {
                    Self::remove_artifacts_for_date(
                        &repo,
                        &project_id,
                        date,
                        datapoint,
                    )?;
                }
            }
        }
        Ok(())
    }

    fn remove_artifacts_for_date(
        repo: &data::Repository,
        project_id: &str,
        date: NaiveDate,
        mut datapoint: data::DataPoint,
    ) -> Result<()> {
        for ref mut entry in datapoint.values_mut() {
            /*
            for artifact_relative_path in entry.artifacts.drain(..) {
                let path = repo
                    .project_datapoint_characteristic_artifact_path(
                        &project_id,
                        date,
                        &characteristic_id,
                        &artifact_relative_path,
                    );
                std::fs::remove_file(&path)?;
            }
            */
            entry.artifacts.clear();
        }
        repo.store_project_datapoint(&project_id, date, &datapoint)?;

        let path = repo
            .project_datapoint_artifacts_directory_path(&project_id, date);
        if path.exists() {
            std::fs::remove_dir_all(&path)?;
        }

        Ok(())
    }

    fn should_remove_artifacts_for_date(&self, date: NaiveDate) -> bool {
        match (self.since, self.until) {
            (Some(since), Some(until)) => date >= since && date <= until,
            (Some(since), None) => date >= since,
            (None, Some(until)) => date <= until,
            (None, None) => true,
        }
    }
}
