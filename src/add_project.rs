use crate::data;
use anyhow::{bail, Result};
use std::path::Path;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct Args {
    /// The id of the project which gets added (e.g. "my_project")
    project_id: String,

    /// The human-readable name of the project (e.g. "My Project")
    #[structopt(long)]
    name: String,

    /// The name of the characteristic (e.g. "My small demo project")
    #[structopt(long)]
    description: Option<String>,

    /// The vcs url (e.g. "git@git.example.com/my/project.git")
    #[structopt(long)]
    vcs: Option<String>,

    /// The website url (e.g. "https://myproject.example.com/")
    #[structopt(long)]
    website: Option<String>,
}

impl Args {
    pub fn process(&self, working_directory: &Path) -> Result<()> {
        let repo = data::Repository::open(working_directory)?;

        if repo.has_project(&self.project_id) {
            bail!(
                "A project with the id {:?} already exists",
                self.project_id
            );
        }

        let project = data::ProjectDescription {
            name: self.name.to_string(),
            description: self.description.clone().unwrap_or_default(),
            vcs: self.vcs.clone().unwrap_or_default(),
            website: self.website.clone().unwrap_or_default(),
        };

        repo.store_project_description(&self.project_id, &project)?;

        Ok(())
    }
}
