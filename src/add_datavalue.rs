use anyhow::{bail, ensure, Context, Result};
use brevdash_data as data;
use chrono::{naive::NaiveDate, Utc};
use log::info;
use std::path::{Path, PathBuf};
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct Args {
    /// The id of the project to which the value gets added
    #[structopt(long = "project")]
    project_id: String,

    /// The id of the characteristic
    #[structopt(long = "characteristic")]
    characteristic_id: String,

    /// The date on which to add the value (default is current system date)
    #[structopt(long)]
    date: Option<NaiveDate>,

    /// The value which gets added
    #[structopt(long)]
    value: data::DataValue,

    /// Artifacts which should be added to the datapoint.
    ///
    /// Provide this parameter multiple times for adding more than one
    /// artifact. The file will be copied into the repo. If you provide
    /// multiple files with the same name, it will fail.
    #[structopt(long = "artifact")]
    artifacts: Vec<PathBuf>,
}

impl Args {
    pub fn process(&self, working_directory: &Path) -> Result<()> {
        let repo = data::Repository::open(working_directory)?;

        // Check that the project exists
        repo.load_project_description(&self.project_id)?;

        let characteristic = repo
            .description
            .characteristics
            .get(&self.characteristic_id)
            .with_context(|| {
                format!(
                    "Couldn't find characteristic \
                description {:?} in brevdash.toml file",
                    self.characteristic_id
                )
            })?;

        if self.value.datatype() != characteristic.datatype {
            bail!(
                "Found datatype {:?} for characteristic {:?}, required datatype {:?}",
                self.value.datatype(),
                self.characteristic_id,
                characteristic.datatype
            )
        }

        let now = Utc::today().naive_local();
        let date = self.date.unwrap_or(now);

        info!(
            "Adding datapoint for characteristic {:?} \
            to project {:?} for {}",
            self.characteristic_id, self.project_id, now
        );

        let mut datapoint =
            if repo.project_has_datapoint_date(&self.project_id, date) {
                repo.load_project_datapoint(&self.project_id, date)?
            } else {
                data::DataPoint::default()
            };

        let mut artifacts = Vec::new();

        for artifact in &self.artifacts {
            ensure!(
                artifact.exists(),
                format!("Artifact at path {:?} doesn't exist", artifact)
            );
            ensure!(
                artifact.is_file(),
                format!("Artifact at path {:?} is not a file", artifact)
            );

            let file_name = artifact.file_name().context(format!(
                "Couldn't get file name for artifact path {:?}",
                artifact
            ))?;

            let dir = repo
                .project_datapoint_characteristic_artifacts_directory_path(
                    &self.project_id,
                    date,
                    &self.characteristic_id,
                );
            std::fs::create_dir_all(dir)?;

            let target_path = repo
                .project_datapoint_characteristic_artifact_path(
                    &self.project_id,
                    date,
                    &self.characteristic_id,
                    &Path::new(file_name),
                );

            std::fs::copy(artifact, target_path)?;

            artifacts.push(Path::new(file_name).to_path_buf());
        }

        let entry = data::DataEntry {
            value: self.value,
            artifacts,
        };

        datapoint.insert(self.characteristic_id.to_string(), entry);

        repo.store_project_datapoint(&self.project_id, date, &datapoint)?;

        Ok(())
    }
}
