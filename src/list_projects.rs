use crate::data;
use anyhow::Result;
use std::path::Path;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct Args {}

impl Args {
    pub fn process(&self, working_directory: &Path) -> Result<()> {
        let repo = data::Repository::open(working_directory)?;

        for id in repo.load_project_ids()? {
            println!("{}", id);
        }

        Ok(())
    }
}
