#![deny(
    missing_debug_implementations,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications
)]

use anyhow::{bail, Result};
use brevdash_data as data;
use chrono::naive::NaiveDate;
use log::debug;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use std::path::{Path, PathBuf};
use structopt::StructOpt;

mod add_characteristic;
mod add_datavalue;
mod add_project;
mod init;
mod list_characteristics;
mod list_projects;
mod remove_artifacts;
mod report;
mod show_project;
mod write_shell_completions;

const PROGRAM_VERSION: &str = env!("CARGO_PKG_VERSION");
const PROGRAM_NAME: &str = env!("CARGO_PKG_NAME");
const PROGRAM_HOMEPAGE: &str = env!("CARGO_PKG_HOMEPAGE");

#[derive(Serialize, Deserialize, PartialEq, Eq, Clone, Copy, Debug)]
pub enum OutputFormat {
    Toml,
    Json,
}

impl Default for OutputFormat {
    fn default() -> Self {
        Self::Toml
    }
}

impl std::str::FromStr for OutputFormat {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "toml" => Ok(OutputFormat::Toml),
            "json" => Ok(OutputFormat::Json),
            _ => bail!("Couldn't parse output format {:?}", s),
        }
    }
}

#[derive(Debug, StructOpt)]
enum Command {
    /// Generate a report
    Report(report::Args),

    /// Initialize a brevdash repository
    Init(init::Args),

    /// Add a data to the repository
    AddDatavalue(add_datavalue::Args),

    /// Add a characteristic to the repository
    AddCharacteristic(add_characteristic::Args),

    /// Add a project to the repository
    AddProject(add_project::Args),

    /// List characteristics in the repository
    ListCharacteristics(list_characteristics::Args),

    /// List projects in the repository
    ListProjects(list_projects::Args),

    /// Remove artifacts
    RemoveArtifacts(remove_artifacts::Args),

    /// Show a project from the repository
    ShowProject(show_project::Args),

    /// Write shell completion files
    #[structopt(setting(structopt::clap::AppSettings::Hidden))]
    WriteShellCompletions(write_shell_completions::Args),
}

#[derive(Debug, StructOpt)]
struct Opt {
    /// Run as if the command was started in this directory instead of the
    /// current working directory.
    #[structopt(long, short = "C", name = "path")]
    change_directory: Option<PathBuf>,

    /// The subcommand
    #[structopt(subcommand)]
    command: Command,
}

impl Opt {
    fn process(&self) -> Result<()> {
        let working_directory = self
            .change_directory
            .clone()
            .unwrap_or_else(|| Path::new(".").to_path_buf());

        match &self.command {
            Command::Report(opt) => opt.process(&working_directory),
            Command::Init(opt) => opt.process(&working_directory),
            Command::AddDatavalue(opt) => opt.process(&working_directory),
            Command::AddProject(opt) => opt.process(&working_directory),
            Command::AddCharacteristic(opt) => {
                opt.process(&working_directory)
            }
            Command::ListCharacteristics(opt) => {
                opt.process(&working_directory)
            }
            Command::ListProjects(opt) => opt.process(&working_directory),
            Command::RemoveArtifacts(opt) => {
                opt.process(&working_directory)
            }
            Command::ShowProject(opt) => opt.process(&working_directory),
            Command::WriteShellCompletions(opt) => opt.process(),
        }
    }
}

#[derive(Debug)]
struct Project {
    description: data::ProjectDescription,
    datapoints: BTreeMap<NaiveDate, data::DataPoint>,
}

fn main() -> Result<()> {
    env_logger::init();

    let opt = Opt::from_args();

    debug!("Parameters: {:#?}", opt);

    opt.process()
}
