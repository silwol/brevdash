use crate::OutputFormat;
use anyhow::Result;
use brevdash_data as data;
use std::path::Path;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct Args {
    /// The output format (default: toml)
    #[structopt(long = "format")]
    output_format: Option<OutputFormat>,

    /// The id of the project
    project_id: String,
}

impl Args {
    pub fn process(&self, working_directory: &Path) -> Result<()> {
        let repo = data::Repository::open(working_directory)?;

        let description =
            repo.load_project_description(&self.project_id)?;
        match self.output_format.unwrap_or_default() {
            OutputFormat::Toml => {
                println!("{}", toml::to_string_pretty(&description)?)
            }
            OutputFormat::Json => {
                println!(
                    "{}",
                    serde_json::to_string_pretty(&description)?
                );
            }
        }

        Ok(())
    }
}
